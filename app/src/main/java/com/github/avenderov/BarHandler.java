package com.github.avenderov;

import ratpack.handling.Context;
import ratpack.handling.Handler;

public class BarHandler implements Handler {

    @Override
    public void handle(Context ctx) {
        ctx.getResponse().send("bar");
    }
}
