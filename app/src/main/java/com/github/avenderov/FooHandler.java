package com.github.avenderov;

import ratpack.handling.Context;
import ratpack.handling.Handler;

public class FooHandler implements Handler {

    @Override
    public void handle(Context ctx) {
        ctx.getResponse().send("foo");
    }
}
