package com.github.avenderov.db;

import com.zaxxer.hikari.HikariConfig;

record DbConfig(String url, String username, String password) {

    public HikariConfig toHikariConfig() {
        final var hikariConfig = new HikariConfig();

        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(username);
        hikariConfig.setPassword(password);

        return hikariConfig;
    }
}
