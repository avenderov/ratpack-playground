package com.github.avenderov.db;

import com.zaxxer.hikari.pool.HikariPool;
import ratpack.hikari.HikariHealthCheck;

import javax.inject.Inject;
import javax.inject.Singleton;

@Singleton
public class DbHealthCheck extends HikariHealthCheck {

    private final HikariPool hikariPool;

    @Inject
    public DbHealthCheck(HikariPool hikariPool) {
        this.hikariPool = hikariPool;
    }

    @Override
    public HikariPool getHikariPool() {
        return hikariPool;
    }
}
