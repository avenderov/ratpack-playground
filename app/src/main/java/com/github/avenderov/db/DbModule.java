package com.github.avenderov.db;

import com.google.inject.Provides;
import com.zaxxer.hikari.HikariConfig;
import org.flywaydb.core.Flyway;
import ratpack.hikari.HikariModule;
import ratpack.server.ServerConfig;
import ratpack.service.Service;
import ratpack.service.StartEvent;

import javax.inject.Singleton;
import javax.sql.DataSource;

public class DbModule extends HikariModule {

    @Override
    protected HikariConfig createConfig(ServerConfig serverConfig) {
        return serverConfig.get("/db", DbConfig.class).toHikariConfig();
    }

    @Singleton
    @Provides
    public Service dbMigrationService(DataSource dataSource) {
        return new Service() {

            @Override
            public String getName() {
                return "migration-service";
            }

            @Override
            public void onStart(StartEvent event) {
                final var flyway =
                    Flyway.configure()
                        .dataSource(dataSource)
                        .load();

                flyway.migrate();
            }
        };
    }
}
