package com.github.avenderov.db;

import com.github.avenderov.ratpack.handling.RequestIdGenerator;
import ratpack.guice.Guice;
import ratpack.handling.RequestId;
import ratpack.handling.RequestLogger;
import ratpack.health.HealthCheckHandler;
import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;
import ratpack.server.ServerConfig;

public class DbApplication {

    private static final String X_REQUEST_ID_HEADER = "X-Request-Id";

    public static void main(String[] args) throws Exception {
        final var config =
            ServerConfig.builder()
                .development(false)
                .yaml("config.yml")
                .env()
                .baseDir(BaseDir.find())
                .build();

        RatpackServer.start(server ->
            server
                .serverConfig(config)
                .registry(
                    Guice.registry(bindings ->
                        bindings
                            .module(DbModule.class)
                            .bindInstance(
                                RequestId.Generator.class,
                                RequestId.Generator.header(X_REQUEST_ID_HEADER, RequestIdGenerator.INSTANCE))
                            .bind(DbHealthCheck.class)))
                .handlers(chain ->
                    chain
                        .all(RequestLogger.ncsa())
                        .get("health/:name?", new HealthCheckHandler())));
    }

}
