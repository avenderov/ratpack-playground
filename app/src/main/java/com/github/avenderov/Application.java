package com.github.avenderov;

import ratpack.server.BaseDir;
import ratpack.server.RatpackServer;

public class Application {

    public static void main(String[] args) throws Exception {
        RatpackServer.start(server ->
            server
                .serverConfig(config ->
                    config.baseDir(BaseDir.find()))
                .handlers(chain ->
                    chain.all(new Router())));
    }
}
