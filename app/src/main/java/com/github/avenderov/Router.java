package com.github.avenderov;

import ratpack.handling.Context;
import ratpack.handling.Handler;

public class Router implements Handler {

    private final Handler fooHandler = new FooHandler();
    private final Handler barHandler = new BarHandler();

    @Override
    public void handle(Context ctx) {
        final var path = ctx.getRequest().getPath();
        if (path.equals("foo")) {
            ctx.insert(fooHandler);
        } else if (path.equals("bar")) {
            ctx.insert(barHandler);
        } else {
            ctx.next();
        }
    }
}
