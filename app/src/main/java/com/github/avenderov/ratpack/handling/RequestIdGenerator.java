package com.github.avenderov.ratpack.handling;

import ratpack.handling.RequestId;
import ratpack.http.Request;

import java.util.concurrent.ThreadLocalRandom;

public class RequestIdGenerator implements RequestId.Generator {

    public static final RequestId.Generator INSTANCE = new RequestIdGenerator();

    private RequestIdGenerator() {
    }

    @Override
    public RequestId generate(Request request) {
        final var nextLong = ThreadLocalRandom.current().nextLong();
        return RequestId.of(Long.toString(Math.abs(nextLong), Character.MAX_RADIX));
    }
}
