CREATE TABLE IF NOT EXISTS employees
(
    id         BIGSERIAL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    updated_at TIMESTAMP WITH TIME ZONE NOT NULL,

    firstname  TEXT                     NOT NULL,
    lastname   TEXT                     NOT NULL,
    email      TEXT                     NOT NULL,

    PRIMARY KEY (id)
);

CREATE UNIQUE INDEX IF NOT EXISTS employees_email_key ON employees (email);
