package com.github.avenderov;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import ratpack.http.Status;
import ratpack.test.embed.EmbeddedApp;

import static org.assertj.core.api.Assertions.assertThat;

class RouterTest {

    @ParameterizedTest
    @CsvSource(textBlock = """
        foo,foo
        bar,bar
        """)
    void shouldRouteByPath(String path, String expected) throws Exception {
        EmbeddedApp.fromHandler(new Router())
            .test(client -> {
                final var response = client.getText(path);

                assertThat(response).isEqualTo(expected);
            });
    }

    @Test
    void shouldReturn404IfNoHandlerForPath() throws Exception {
        EmbeddedApp.fromHandler(new Router())
            .test(client -> {
                final var response = client.get();

                assertThat(response.getStatus()).isEqualTo(Status.NOT_FOUND);
            });
    }
}
